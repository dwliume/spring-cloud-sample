package me.dwliu.springcloud.ch1801customer.controller;

import me.dwliu.springcloud.ch1801customer.feign.HelloFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class HelloController {
    @Autowired
    private HelloFeignService helloFeignService;

    @GetMapping("/{name}")
    public String hello(@PathVariable String name) {
        String s = helloFeignService.helloService(name);

        return s + LocalDateTime.now();


    }
}

