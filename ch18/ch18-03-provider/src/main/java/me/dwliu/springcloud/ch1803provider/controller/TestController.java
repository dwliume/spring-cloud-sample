package me.dwliu.springcloud.ch1803provider.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @GetMapping("v1")
    public String v1() {
        return "V1";
    }


    @GetMapping("v2")
    public String v2() {
        return "V2";
    }
}
