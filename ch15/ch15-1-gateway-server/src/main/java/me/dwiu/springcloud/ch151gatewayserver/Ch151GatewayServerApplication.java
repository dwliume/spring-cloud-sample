package me.dwiu.springcloud.ch151gatewayserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Ch151GatewayServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch151GatewayServerApplication.class, args);
    }
}
