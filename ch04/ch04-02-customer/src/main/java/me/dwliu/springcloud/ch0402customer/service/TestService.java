package me.dwliu.springcloud.ch0402customer.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "ch04-02-provider")
public interface TestService {

    @GetMapping("test")
    String testProvider();
}
