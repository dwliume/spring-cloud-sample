package me.dwliu.springcloud.ch1702gateway1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch1702Gateway1Application {

    public static void main(String[] args) {
        SpringApplication.run(Ch1702Gateway1Application.class, args);
    }
}
