package me.dwliu.springcloud.ch0402customer.service;

import me.dwliu.springcloud.ch0402customer.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "ch04-02-provider")
public interface UserService {

    @GetMapping("/user/add")
    User addUser(User user);

    @PostMapping("/user/update")
    User updateUser(@RequestBody User user);
}
