package me.dwliu.springcloud.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {
    public final String remoteUri = "http://localhost:8080";

    @Bean
    public RouteLocator headRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("add_request_header_route", r ->
                        r.path("/test1")
                                .filters(f -> f.addRequestHeader("X-Request-Acme", "ValueB"))
                                .uri(remoteUri + "/test/header"))
                .build();
    }

    @Bean
    public RouteLocator requestRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("add_request_parameter_route"
                        , r -> r.path("/request1")
                                .filters(f -> f.addRequestParameter("username", "liudw"))
                                .uri(remoteUri + "/test/request"))
                .build();
    }

}
