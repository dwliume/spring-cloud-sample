package me.dwliu.springcloud.ch1801provider;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Ch1801ProviderApplicationTests {

    @Test
    public void contextLoads() {
    }

}
