package me.dwliu.springcloud.ch0401gzip.controller;

import me.dwliu.springcloud.ch0401gzip.service.HelloFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloFeignController {

    @Autowired
    private HelloFeignService helloFeignService;

    @GetMapping("/search/github")
    public ResponseEntity<byte[]> test(@RequestParam("q") String q) {
        ResponseEntity<byte[]> hello = helloFeignService.hello(q);

        return hello;

    }
}
