package me.dwliu.springcloud.ch3eurekaclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Ch3EurekaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch3EurekaClientApplication.class, args);
    }
}
