package me.dwliu.springcloud.ch0402provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Ch0402ProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch0402ProviderApplication.class, args);
    }
}
