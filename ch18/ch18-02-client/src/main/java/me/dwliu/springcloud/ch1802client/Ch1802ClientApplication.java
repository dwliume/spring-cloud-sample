package me.dwliu.springcloud.ch1802client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch1802ClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch1802ClientApplication.class, args);
    }
}
