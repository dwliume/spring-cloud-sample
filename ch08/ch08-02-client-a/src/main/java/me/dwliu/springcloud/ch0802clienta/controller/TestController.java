package me.dwliu.springcloud.ch0802clienta.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@RestController
public class TestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @GetMapping("/header")
    public String getHeader(HttpServletRequest request) {
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String element = headerNames.nextElement();
            //LOGGER.info("----------------element:{}", element);
            LOGGER.info("----------------key-value:{}={}", element, request.getHeader(element));

        }
        LOGGER.info("==================");
        return "hello";
    }
}
