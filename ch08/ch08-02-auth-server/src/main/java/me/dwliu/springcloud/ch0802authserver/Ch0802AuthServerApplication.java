package me.dwliu.springcloud.ch0802authserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Ch0802AuthServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch0802AuthServerApplication.class, args);
    }
}
