package me.dwliu.springcloud.ch0701zuulserver.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @GetMapping("test1")
    public String test() {

        return "test zuul";

    }
}
