package me.dwliu.springcloud.ch151providerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Ch151ProviderServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch151ProviderServiceApplication.class, args);
    }
}
