package me.dwliu.springcloud.ch1703gatewaytest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch1703GatewayTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch1703GatewayTestApplication.class, args);
    }
}
