package me.dwliu.springcloud.ch0701zuulserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class Ch0701ZuulServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch0701ZuulServerApplication.class, args);
    }
}
