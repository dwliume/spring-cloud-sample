package me.dwliu.springcloud.ch0402customer.controller;

import me.dwliu.springcloud.ch0402customer.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestCustomerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestCustomerController.class);
    @Autowired
    private TestService testService;

    @RequestMapping("test-customer")
    public String test() {
        LOGGER.info("============TestCustomerController=============");

        return testService.testProvider();

    }
}
