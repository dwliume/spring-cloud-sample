package me.dwliu.springcloud.ch0501ribbonloadbalancer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class TestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private RestTemplate restTemplate;


    @GetMapping("add")
    public String add(Integer a, Integer b) {
        String result = restTemplate.getForObject("http://client-a/add?a=" + a + "&b=" + b, String.class);
        LOGGER.info("ribbon-loadbalance:{}", result);

        return result;

    }
}
