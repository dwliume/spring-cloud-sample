package me.dwliu.springcloud.ch1802gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch1802GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch1802GatewayApplication.class, args);
    }
}
