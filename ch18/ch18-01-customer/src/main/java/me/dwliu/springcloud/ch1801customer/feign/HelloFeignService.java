package me.dwliu.springcloud.ch1801customer.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "provider")
public interface HelloFeignService {

    @GetMapping("hello")
    String helloService(@RequestParam("name") String name);
}
