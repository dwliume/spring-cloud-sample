package me.dwliu.springcloud.ch1803provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch1803ProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch1803ProviderApplication.class, args);
    }
}
