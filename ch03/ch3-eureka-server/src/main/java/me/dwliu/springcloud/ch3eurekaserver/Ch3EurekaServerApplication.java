package me.dwliu.springcloud.ch3eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Ch3EurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch3EurekaServerApplication.class, args);
    }
}
