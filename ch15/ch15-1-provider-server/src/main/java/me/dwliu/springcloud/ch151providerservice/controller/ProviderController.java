package me.dwliu.springcloud.ch151providerservice.controller;

import me.dwliu.springcloud.util.UserContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ProviderController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProviderController.class);

    @RequestMapping("provider/test")
    public String test(HttpServletRequest request) {
        LOGGER.debug(UserContextHolder.currentUser().getUsername());
        return "provider -server";

    }
}
