package me.dwliu.springcloud.ch0701clienta.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("add")
    public String add(Integer a, Integer b) {
        return (a + b) + "";
    }
}
