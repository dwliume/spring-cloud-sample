package me.dwliu.springcloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch1701Gateway1Application {

    public static void main(String[] args) {
        SpringApplication.run(Ch1701Gateway1Application.class, args);
    }
}
