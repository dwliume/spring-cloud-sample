package me.dwliu.springcloud.ch0402customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class Ch0402CustomerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch0402CustomerApplication.class, args);
    }
}
