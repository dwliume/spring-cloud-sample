package me.dwliu.springcloud.ch0401hello.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HelloFeignConfig {

    @Bean
    public Logger.Level level() {
        return Logger.Level.FULL;
    }
}
