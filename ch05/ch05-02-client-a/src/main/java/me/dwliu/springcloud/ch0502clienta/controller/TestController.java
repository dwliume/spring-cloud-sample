package me.dwliu.springcloud.ch0502clienta.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class TestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @RequestMapping("add")
    public String test(Integer a, Integer b, HttpServletRequest request) {
        LOGGER.info("==============server port:{},result:{}", request.getServerPort(), (a + b));


        return request.getServerPort() + "";
    }
}
