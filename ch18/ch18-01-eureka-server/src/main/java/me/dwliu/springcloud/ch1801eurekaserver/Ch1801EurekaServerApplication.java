package me.dwliu.springcloud.ch1801eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Ch1801EurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch1801EurekaServerApplication.class, args);
    }
}
