package me.dwliu.springcloud.ch0401hello.controller;

import me.dwliu.springcloud.ch0401hello.service.HelloFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloFeignController {

    @Autowired
    private HelloFeignService helloFeignService;

    @GetMapping("/search/github")
    public String test(@RequestParam("q") String q) {
        String hello = helloFeignService.hello(q);

        return hello;

    }
}
