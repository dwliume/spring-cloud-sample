package me.dwliu.springcloud.util;

import me.dwliu.springcloud.vo.User;

public class UserContextHolder {

    public static ThreadLocal<User> context = new ThreadLocal<>();

    public static User currentUser() {
        return context.get();
    }

    public static void revome() {
        context.remove();
    }

    public static void set(User user) {
        context.set(user);
    }


}
