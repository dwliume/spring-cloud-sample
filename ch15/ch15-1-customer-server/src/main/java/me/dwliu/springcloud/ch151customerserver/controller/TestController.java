package me.dwliu.springcloud.ch151customerserver.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@RestController
public class TestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);
    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("test")
    public String test(HttpServletRequest request) {
        Enumeration<String> headerNames = request.getHeaderNames();
        String s = "";
        while (headerNames.hasMoreElements()) {
            s = headerNames.nextElement();
            LOGGER.debug("-----------key:{}----------", s);

        }

        return s;
    }


    @RequestMapping("accessProvider")
    public String accessProvider(HttpServletRequest request) {
        String result = restTemplate.getForObject("http://provider-server/provider/test", String.class);
        return result;

    }

}
