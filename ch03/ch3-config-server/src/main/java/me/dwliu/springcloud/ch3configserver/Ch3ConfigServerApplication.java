package me.dwliu.springcloud.ch3configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class Ch3ConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch3ConfigServerApplication.class, args);
    }
}
