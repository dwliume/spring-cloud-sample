package me.dwliu.springcloud.ch0401hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class Ch0401HelloApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch0401HelloApplication.class, args);
    }
}
