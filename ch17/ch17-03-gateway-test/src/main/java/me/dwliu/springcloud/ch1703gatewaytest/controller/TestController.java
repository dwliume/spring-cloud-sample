package me.dwliu.springcloud.ch1703gatewaytest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("test")
public class TestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @GetMapping("/header")
    public String testHeader(HttpServletRequest request) {
        String header = request.getHeader("X-Request-Acme");
        LOGGER.info("请求头：{}", header);

        return header;
    }

    @RequestMapping("request")
    public String testRequest(HttpServletRequest request) {
        LOGGER.info("请求的参数：{}", request.getParameter("username"));
        return request.getParameter("username");
    }

    @RequestMapping("response")
    public String testResponse(HttpServletResponse response) {
        LOGGER.info("请求的参数：{}", response.getHeader("X-Response-Foo"));
        return response.getHeader("X-Response-Foo");
    }
}
