package me.dwliu.springcloud.ch0502clienta;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Ch0502ClientAApplicationTests {

    @Test
    public void contextLoads() {
    }

}
