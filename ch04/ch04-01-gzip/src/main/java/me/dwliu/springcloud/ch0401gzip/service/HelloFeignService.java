package me.dwliu.springcloud.ch0401gzip.service;

import me.dwliu.springcloud.ch0401gzip.config.HelloFeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "github-server", url = "https://api.github.com", configuration = HelloFeignConfig.class)
public interface HelloFeignService {

    @GetMapping("/search/repositories")
    ResponseEntity<byte[]> hello(@RequestParam("q") String q);
}
