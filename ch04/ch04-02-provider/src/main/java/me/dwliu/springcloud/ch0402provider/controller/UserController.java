package me.dwliu.springcloud.ch0402provider.controller;

import me.dwliu.springcloud.ch0402provider.model.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    @GetMapping("/add")
    public User add(User user) {
        user.setId(111L);
        return user;
    }


    @PostMapping("/update")
    public User update(@RequestBody User user) {
        return user;
    }
}
