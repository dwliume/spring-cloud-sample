package me.dwliu.springcloud.ch1803gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch1803GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch1803GatewayApplication.class, args);
    }
}
