package me.dwliu.springcloud.ch0501clienta.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class TestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);


    @GetMapping("add")
    public String test(Integer a, Integer b, HttpServletRequest request) {
        LOGGER.info("From port:{} ; Result:{}", request.getServerPort(), (a + b));
        return "From port:" + request.getServerPort() + ";Result:" + (a + b);
    }
}

