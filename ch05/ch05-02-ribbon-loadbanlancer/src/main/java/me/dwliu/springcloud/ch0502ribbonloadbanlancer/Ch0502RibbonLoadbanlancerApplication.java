package me.dwliu.springcloud.ch0502ribbonloadbanlancer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Ch0502RibbonLoadbanlancerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch0502RibbonLoadbanlancerApplication.class, args);
    }
}
