package me.dwliu.springcloud.vo;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {
    public static final String USER_ID = "x-user-id";
    public static final String USER_NAME = "x-user-name";


    private String userId;
    private String username;

    private List<String> allowPermissionService;


    public User() {
    }

    public User(String userId, String username) {
        this.userId = userId;
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getAllowPermissionService() {
        return allowPermissionService;
    }

    public void setAllowPermissionService(List<String> allowPermissionService) {
        this.allowPermissionService = allowPermissionService;
    }
}
