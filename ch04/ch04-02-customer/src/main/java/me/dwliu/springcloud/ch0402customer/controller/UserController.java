package me.dwliu.springcloud.ch0402customer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import me.dwliu.springcloud.ch0402customer.model.User;
import me.dwliu.springcloud.ch0402customer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cus/user")
@Api(tags = "用户管理")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiOperation("保存")
    @PostMapping("add")
    public User addUser(@RequestBody @ApiParam User user) {

        return userService.addUser(user);
    }

    @ApiOperation("修改")
    @PostMapping("update")
    public User updateUser(@RequestBody @ApiParam User user) {

        return userService.updateUser(user);
    }


}
