package me.dwliu.springcloud.ch1802gateway.config;

import me.dwliu.springcloud.ch1802gateway.filter.CustomerGatewayFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    @Bean
    public RouteLocator FilterRoteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("filter-route", r -> r.path("/test")
                        .filters(f -> f.filter(new CustomerGatewayFilter()))
                        .uri("http://localhost:8080/hello?name=liudw")
                        .order(0)
                )
                .build();
    }
}
