package me.dwiu.springcloud.ch151gatewayserver.controller;

import me.dwiu.springcloud.ch151gatewayserver.util.JwtUtil;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {

    @RequestMapping("/getToken/{name}")
    public String getToken(@PathVariable String name) {
        return JwtUtil.generateToken(name);
    }
}
