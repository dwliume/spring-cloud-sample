package me.dwliu.springcloud.ch0501ribbonloadbalancer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Ch0501RibbonLoadbalancerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch0501RibbonLoadbalancerApplication.class, args);
    }
}
