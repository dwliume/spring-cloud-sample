package me.dwliu.springcloud.ch1702gateway1.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Configuration
public class GatewayConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(GatewayConfig.class);

    //@Bean
    public RouteLocator afterRouteLocate(RouteLocatorBuilder builder) {
        ZonedDateTime zonedDateTime = LocalDateTime.now().minusHours(1).atZone(ZoneId.systemDefault());

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String format = zonedDateTime.format(dateTimeFormatter);

        LOGGER.info("指定时间：{}", format);

        // 当前请求时间比指定时间晚放行，eg:请求时间为13:00 指定时间为12:00，放行
        return builder.routes()
                .route("after_route", r -> r.after(zonedDateTime).uri("http://example.com"))
                .build();
    }

    //@Bean
    public RouteLocator beforeRouteLoactor(RouteLocatorBuilder builder) {

        ZonedDateTime zonedDateTime = LocalDateTime.now().plusHours(2).atZone(ZoneId.systemDefault());


        return builder.routes()
                .route("before_route",
                        r -> r.before(zonedDateTime).uri("http://example.com"))
                .build();
    }

    //@Bean
    public RouteLocator betweenRouteLocator(RouteLocatorBuilder builder) {
        ZonedDateTime datetime1 = LocalDateTime.now().minusHours(1).atZone(ZoneId.systemDefault());

        ZonedDateTime datetime2 = LocalDateTime.now().plusHours(1).atZone(ZoneId.systemDefault());
        return builder.routes()
                .route("between_route",
                        r -> r.between(datetime1, datetime2)
                                .uri("http://examle.com"))
                .build();
    }

    //@Bean
    public RouteLocator cookieRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("cookie_route", r -> r.cookie("x-user", "liudw").uri("http://jerei.com"))
                .build();
    }

    //@Bean
    public RouteLocator headerRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("header_route",
                        r -> r.header("x-header", "liudw")
                                .uri("http://example.com"))
                .build();
    }

    //@Bean
    public RouteLocator hostRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("host_route", r -> r.host("**.example.com:7777").uri("http://baidu.com"))
                .build();
    }

    //    @Bean
    public RouteLocator methodRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("method_route", r -> r.method(HttpMethod.POST).uri("http://jerei.com")).build();
    }

    //    @Bean
    public RouteLocator queryRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("query_route", r -> r.query("q", "1").uri("http://jd.com")).build();
    }

    @Bean
    public RouteLocator remoteAddrRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("remoteAddr_route", r -> r.remoteAddr("localhost").uri("http://jd.com"))
                .build();
    }

}
