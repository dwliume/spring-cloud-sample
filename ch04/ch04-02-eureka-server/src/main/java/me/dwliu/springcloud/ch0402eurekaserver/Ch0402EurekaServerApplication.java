package me.dwliu.springcloud.ch0402eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Ch0402EurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch0402EurekaServerApplication.class, args);
    }
}
